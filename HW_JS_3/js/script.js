//1. Выведите числа от 1 до 50 и от 35 до 8.

// let i = 1;
// while (i <= 50) {
//     document.write (i + ' ');
//     i++;
// }



//  i = 35;
// while (i >=8) {
//     document.write (i + ' ');
//     i--;
// }


// 2. Выведите столбец чисел от 89 до 11 - воспользуйтесь циклом while и отделите числа тегом <br /> друг от друга, чтобы получить столбец, а не строку.

//  i = 89;
// while (i >=11) {
//     document.write (i  + '<br>' );
//     i--;
// }


// 3. С помощью цикла найдите сумму чисел от 0 до 100.

// let sum = 0;
// for (let i = 0; i <= 100; i++){
// sum += i;
// }
// document.write (sum)





// 4. Найдите сумму чисел в каждом числе от 1 до 5, например: в числе 3 сумма составляет 6
// (1+2+3).


// for (let i = 1; i <= 5; i++) {
//     let sum = 0;
//     for (let i2 = 1; i2 <= i; i2++){
//     sum += i2;
//     }
//     console.log (sum)
//   }





// 5. Выведите чётные числа от 8 до 56. Решить задание через while и for.

// for (let i = 8; i<=56; i++) {
//     if (i % 2 ==0){
//         console.log (i);
//     }

// }


// 6. Необходимо вывести на экран полную таблицу умножения (от 2 до 10) в виде:
// 2*2 = 4
// 2*3 = 6
// 2*4 = 8
// 2*5 = 10
// ...
// 3*1=3
// 3*2=6
// 3*3=9
// 3*4=12
// ...
// Для решения задачи используйте вложенные циклы.

// for (i = 1; i <= 9; i++){  
// document.write("<div>"); 
// for (j = 1; j <=9; j++)  
// { 
// document.write(i + " * " + j + " = " +(i*j) + "<br>"); 
// }  
// document.write ("</div>"); 
// }





// 7. Дано число n=1000. Делите его на 2 столько раз, пока результат деления не станет
// меньше 50. Какое число получится? Посчитайте количество итераций, необходимых
// для этого (итерация - это проход цикла), и запишите его в переменную num.

// let num = 0;
//  for (i=1000; i>=50; i++){
//     i = (i/2);
//     num++
//  }
//  document.write (i + "</br>")
//  document.write (num)


// 8. Запустите цикл, в котором пользователю предлагается вводить число с клавиатуры, до
// тех пор, пока не будет введена пустая строка или 0. После выхода из цикла выведите
// общую сумму и среднее арифметическое введённых чисел. Если пользователь ввел не
// число, то вывести сообщение об ошибке ввода. При подсчете учесть, что пользователь
// может ввести отрицательное значение.

// let sum = 0;
// let avg = 0;
// let count = 0;


// for ( ; ; ) {
//     const number = +prompt('Введите число');
//     if (isNaN (number)=== true) {
//      document.write (' <h3> Ошибка ввода</h3> ')
//      break;
//     }
//     if (number === '' || number ===0){
//       break;
//     }

//     count +=1;
//     sum += number;
//     avg = sum/count;


//  }
//     document.write(`<h2>Sum: ${sum}</h2>`)
//     document.write(`<h2>Avg: ${avg}</h2>`)


// 9. Дана строка с числами разделенными пробелами «4 98 4 6 1 32 4 65 4 3 5 7 89 7 10 1 36
// 8 57». Найдите самое большое и самое маленькое число в строке, используя цикл.

// const str = '4 98 4 6 1 32 4 65 4 3 10000 5 7 89 7 10 1 -100 36 8 57'
// let maxNumber = -Infinity;
// let minNumber = +Infinity;
// let strNumber = '';

// for (let i = 0; i < str.length+1; i++) {
//   if (str[i] !== ' ' && str [i] !== undefined){
//     strNumber += str[i]
//   }else {
//     let strToNumber = +strNumber
//     maxNumber = maxNumber < strToNumber ? strToNumber : maxNumber;
//     minNumber = minNumber > strToNumber ? strToNumber : minNumber;
//     strNumber = ''
//   }
// }

// console.log (maxNumber);
// console.log (minNumber);



// 2 вариант  сплит

// const str = '4 98 4 6 1 32 4 65 4 3 5 7 89 7 10 1 36 8 57'
// const arr = str.split(' ')
// let maxNumber = -Infinity;
// let minNumber = +Infinity;


// for (let i = 0; i < arr.length; i++) {
//     let strToNumber = +arr[i]
//     maxNumber = maxNumber < strToNumber ? strToNumber : maxNumber;
//     minNumber = minNumber > strToNumber ? strToNumber : minNumber;
// }

// console.log (maxNumber);
// console.log (minNumber);

// 10. Дано произвольное целое число n. Написать программу, которая:
// a. разбивает число n на цифры и выводит их на экран;
// b. подсчитывает сколько цифр в числе n;
// c. находит сумму цифр числа n;
// d. меняет порядок цифр числа n на обратный.
// Пример: вводится число 123: цифр в числе = 3; сумма = 6; обратный порядок 321.


// const number = 12345;
// const strNumber = `${number}`;
// let counter = 0;
// let numberRevStr = '';
// let sum = 0;

// for (let i = 0; i < strNumber.length; i++ ) {
//     console.log (+strNumber[i])
//     sum += +strNumber[i]
//     counter += 1;
//     numberRevStr += strNumber[strNumber.length -1 - i]
// }

// console.log (`count: ${counter}`);
// console.log (`sum: ${sum}`);
// console.log (`NumRev: ${numberRevStr}`);

