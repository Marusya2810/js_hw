// Лампочка. Возможные методы: ввод информации о лампочке (мощность, стоимость
//     электроэнергии), вкл./выкл. лампочки, получение расхода за горение лампочки,
//     счетчик горения лампочки.

let light = function (name) {

    this.get = function () {
    this.name = name;
    this.power = +prompt ('Введите мощность лампочки');
    this.cost = +prompt ('Введите стоимость электроэнергии за 1 Квт');
    if (confirm('Включить лампочку?')) {
        this.workTime = prompt ('Сколько времени работает лампочка?')
    } else {
        this.workTime = 0;
    }
    this.operation ();
    }

    this.operation = function () {
    this.result = (this.power/1000)*this.cost*this.workTime;
    this.show ();
    }

    this.show = function () {
    if (this.workTime === 0 || this.workTime == undefined ) {
        console.log ('Лампочка' + this.name + 'не включена')
    } else {
        console.log ('Лампочка ' + this.name + ' проработала ' +  this.workTime +  "  часов, стоимость электроэнергии составляет " + this.cost + 'p.')
    }

    }
}

let light1 = new light ('Гостинная');
light1.get ();

let light2 = new light ('Спальня');
light2.get ();

let light3 = new light ('Балкон');
light3.get ();

let total_amount = light1.result + light2.result + light3.result;
console.log ('Итоговая сумма составляет: ' + total_amount);


